var data = {
	items: [
		{id: 1, label: "1 item"},
		{id: 2, label: "2 item", items: [
			{id: 3, label: "2.1 item"}
		]},
		{id: 4, label: "3 item", items: [
			{id: 5, label: "3.1 item", items: [
				{id: 6, label: "3.1.1 item"},
				{id: 7, label: "3.1.2 item"}
			]},
			{id: 8, label: "3.2 item"}
		]}
	]};