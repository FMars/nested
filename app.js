/**
 * Записываем сгенерированый html код в DOM елемент заменяя его содержимое
 *
 * @param Object data объект дерева
 * @param string elementId id DOM елемента, по-умолчанию `wrapper`
 **/
var render = function(data, elementId) {

	/**
	 * Получаем наши items в виде html кода
	 *
	 * @param Array items массив объектов `items`
	 * @return string html код
	 **/
	var __getHtml = function(items) {
		var html = '<div class="ui list">';
		
		for (var i in items) {
		
			if (items[i] !== undefined) {
				html += '<div class="item"><i class="caret right icon"></i><div class="content">'+
					'<div class="header">ID: '+items[i].id+'</div><div class="description">'+items[i].label+'</div>';

				if (items[i].hasOwnProperty('items')) {
					html += __getHtml(items[i].items);
				}
			
				html += '</div></div>';
			}
		}
		
		return html + '</div>';
	}
	
	var elementId = typeof elementId !== 'undefined' ?  elementId : 'wrapper';
	document.getElementById(elementId).innerHTML = __getHtml(data.items);
}


/**
 * Получаем все ids у items
 *
 * @param Object data объект дерева
 * @param boolean asObject true если нужен объект с ключами, по-умолчанию false
 * @return Array отсортированый массив ids
 **/
var getIds = function (data, asObject) {

	/**
	* Получаем все ids у items рекурсивным обходом
	*
	* @param Object items массив объектов
	* @return Object объект с полями ids
	**/
	var __getIds = function(items) {
		var ids = {};
		
		for (var i in items) {
			
			if ((typeof items[i]) == 'object') {
				var obj = __getIds(items[i]);
				for (var x in obj) {
					if (obj.hasOwnProperty(x) && x.slice(-2) == 'id')
						ids[i + '.' + x] = obj[x];
				}
			} else {
				ids[i] = items[i];
			}
		}
		return ids;
	};

	var asObject = typeof asObject !== 'undefined' ?  asObject : false;
	var data = __getIds(data.items);

	// Вернуть объект
	if (asObject)
		return data;

	// Переносим ids в массив
	var ids  = [];
	for(key in data) {
	    ids.push(data[key]);
	}

	// Анонимная функция для корректной сортировки чисел
	return ids.sort(function(a, b) {
		return a - b;
	});
}


/**
 * Рендерим select'ы
 *
 * @param Array ids массив значений для select`а
 * @param string selectClass класс для select`ов которые нужно сгенерировать, по-умолчанию `select`
 **/
var renderSelects = function(ids, selectClass) {
	var selectClass = typeof selectClass !== 'undefined' ?  selectClass : 'select';
	var selects     = document.querySelectorAll('.' + selectClass)
	var html        = '';

	// Генерируем options
	for (var i = 0; i < ids.length; ++i) {
		if (ids[i] !== undefined)
			html += '<option value="' + ids[i] + '">' + ids[i] + '</option>';
	}

	// Записываем options в select`ы
	for (var i = 0; i < selects.length; ++i) {
		var __html = html;

		// Добавляем 'root' если это нужно
		if (selects[i].id == 'action-add-id' || selects[i].id == 'action-move-parent-id') {
			__html = '<option value="0">ROOT</option>' + __html;
		}

		selects[i].innerHTML = __html;
	}
}


/**
 * Меняем форму в зависимости от действия
 *
 * @param Object event событие  
 **/
var changeForm = function(event) {
	var currentActionsInputs = document.querySelectorAll('.action-' + event.target.value); 
	var actionInputs         = document.querySelectorAll('.action');

	// Скрываем все inputs
	for (var i = 0; i < actionInputs.length; ++i) {
		actionInputs[i].style.display = "none";
	}

	// Показываем активные inputs
	for (var i = 0; i < currentActionsInputs.length; ++i) {
		currentActionsInputs[i].style.display = "block";
	}
}


/**
 * Выполняем действия над деревом
 *
 * @param Object event событие
 **/
var make = function(event) {

	// Вспомогательная функция для доступа к елементам объекта по строчному ключу: 'data["0.items.1.id"]'
	Object.byString = function(object, key) {
	    var key = key.replace(/\[(\w+)\]/g, '.$1');
	    key = key.replace(/^\./, ''); 
	    var a = key.split('.');
	    for (var i = 0, n = a.length; i < n; ++i) {
	        var k = a[i];
	        if (k in object) {
	            object = object[k];
	        } 
	        else return;
	    }
	    return object;
	}

	/**
	 * Удаляем item из дерева
	 *
	 * @param integer id идентификатор удаляемого элемента
	 * @param boolean withoutChildren удаляем с дочерними элементами или нет
	 **/
	var __delete = function(id, withoutChildren) {
		var keys = getIds(data, true);
		for (var i in keys) {
		  	if (keys[i] == id) {
		  		// куча телодвижений из-за невозможности удалить в javascript объект целиком
		  		var key    = i.slice(0, -3);
		  		var index  = key.slice(-1);
		  		var parent = key.slice(0, -2);
	  			var obj    = Object.byString(data.items, parent);
	  			obj = obj ? obj : data.items;

	  			if (withoutChildren && obj[index].items !== undefined) {
	  				var childrens = obj[index].items;
	  				delete obj[index];
	  				for (var i = 0; i < childrens.length; i++) {
	  					obj[obj.length + i] = childrens[i];
	  				}
	  			} 
	  			else
	  				delete obj[index];
		 	 }
		}
	}


	/**
	 * Перемещаем item по дереву
	 *
	 * @param integer id идентификатор перемещаемого элемента
	 * @param integer id идентификатор родительного элемента (куда перемещать)
	 **/
	var __move = function(id, parentId) {
		var keys   = getIds(data, true);

		for (var i in keys) {
			  	if (keys[i] == id) {
			  		var key = i.slice(0, -3);
			  		var obj = Object.byString(data.items, key);

			  		// Повторно используем уже написанные методы
			  		__delete(id);
			  		__add(parentId, undefined, obj);			  	
			 	}
			}
	}


	/**
	 * Добавляем item в дерево
	 *
	 * @param integer parentId идентификатор родительного элемента (куда перемещать)
	 * @param string label метка нового элемента
	 * @param Object item можно добавить существующий объект 
	 **/
	var __add = function(parentId, label, item) {
		var newId  = getIds(data).pop() + 1;
		var keys   = getIds(data, true);
		var newObj = {};

		if (label !== undefined)
			newObj = {id: newId, label: label};
		else
			newObj = item;

		if (parentId == 0) {
			data.items[data.items.length] = newObj;
		} 
		else {
			for (var i in keys) {
			  	if (keys[i] == parentId) {
			  		var key = i.slice(0, -3);
			  		var obj = Object.byString(data.items, key);
			  		if (obj.items === undefined) {
			  			obj.items = [];			  			
			  		}
			  		obj.items.push(newObj);
			 	}
			}
		}
	}

	var action = document.querySelector('#action').value;
	
	// Add action
	if (action == 'add') {
		var addId    = document.querySelector('#action-add-id').value;
		var addLabel = document.querySelector('#action-add-label').value;

		__add(addId, addLabel);
	}

	// Move action
	else if (action == 'move') {
		var moveItemId   = document.querySelector('#action-move-item-id').value;
		var moveParentId = document.querySelector('#action-move-parent-id').value;

		__move(moveItemId, moveParentId);		
	}

	// Delete action
	else if (action == 'delete') {
		var deleteId       = document.querySelector('#action-delete-id').value;
		var deleteChildren = document.querySelector('#action-delete-children').checked;

		__delete(deleteId, deleteChildren);		
	}

	render(data);
	renderSelects(getIds(data));
}


/**
 * Инициализация приложения
 *
 * @param Object data объект дерева
 **/
var init = function(__data) {
	var defaults = {
		action: { target: { value: 'add'}}, // действие по-умолчанию
	}

	render(__data);                // Инициализация представления объекта дерева 
	renderSelects(getIds(__data)); // Инициализация селектов со всеми возможными значениями
	changeForm(defaults.action);   // Инициализация формы в зависимости от действия по-умолчанию

	// Инициализация слушателей
	document.getElementById("action").addEventListener("change", changeForm);
	document.getElementById("button").addEventListener("click", make);
}


// Запуск приложения
init(data);
